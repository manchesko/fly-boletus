﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBomb : MonoBehaviour
{
    //public GameObject bomb;
    private float leftX = -8.1f;
    private float rightX = 8.1f;
    private float spawnY = 6.5f;

    private void Start()
    {
        StartCoroutine(Spawn ());
    }

    IEnumerator Spawn()
    {        
        {
            yield return new WaitForSeconds(0.7f);
            GameObject bomb = Instantiate(gameObject, new Vector3(Random.Range(leftX, rightX), spawnY),
            Quaternion.identity);            
        }

    }








}




