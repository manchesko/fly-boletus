﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyUp : MonoBehaviour
{
    [SerializeField] private float flyspeed;
    public Vector3 direction;
    private int up = 7;

	public void Update ()
    {
        transform.Translate(direction * flyspeed * Time.deltaTime);
       
        if (transform.position.y > up || gameObject.tag == "Player")
            Destroy(gameObject);
        //Debug.Log(transform.position);
	}
}
