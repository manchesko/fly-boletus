﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallDownBomb : MonoBehaviour
{
    [SerializeField] private float fallspeed;
    public Vector3 direction;
    private int down = -6;
    
	void Update ()
    {
        transform.Translate(direction * fallspeed * Time.deltaTime);

        if (transform.position.y < down)
            Destroy(gameObject);		
	}
}
