﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Player : MonoBehaviour {

    [SerializeField] private float speed;
    [SerializeField] private float jumpforce;
    
    Rigidbody2D rb;
    public SpriteRenderer groundcheck;   
    bool isGrounded;
    public GameGUI gameGUI;
   

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        groundcheck = GetComponentInChildren<SpriteRenderer>();
        
    }

    private void FixedUpdate()
    {
        if (Input.GetButton("Horizontal"))
            Run();
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
            Jump();        
    }

    private void Run()
    {
        Vector3 direction = transform.right * Input.GetAxis("Horizontal");
        transform.position = Vector3.MoveTowards(transform.position, 
        transform.position + direction, speed * Time.deltaTime);

        groundcheck.flipX = direction.x < 0;
    }

    private void Jump()
    {
        rb.AddForce(new Vector3(transform.position.x, jumpforce)/*, ForceMode2D.Impulse*/);
        Debug.Log(jumpforce);
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground")
            isGrounded = true;
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground")
            isGrounded = false;
    }
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Boletus")
        {
            Destroy(other.gameObject);
            gameGUI.AddScore();          
        }    
        if (other.gameObject.tag == "Bomb")            
        SceneManager.LoadScene(0);
    }
}
