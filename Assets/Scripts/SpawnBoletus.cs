﻿using System.Collections;
using UnityEngine;

public class SpawnBoletus : MonoBehaviour
{
    //public GameObject boletus;
    private float leftX = -8.1f;
    private float rightX = 8.1f;
    private float spawnY = -12.3f;
	
	void Start () {
        StartCoroutine(Spawn ());//При использовании корутины, когда уничтожаеться последний объект,
        //клонироваться больше нечему и поток прекращаеться... Проблему можно решить хитрым способом,
        //поместив объекты так чтобы по оси Y их появлялось n-ое количество, до попадания в камеру.
	}

    IEnumerator Spawn()
    {
        {
            yield return new WaitForSeconds(0.5f);
            GameObject boletus = Instantiate(gameObject, new Vector3(Random.Range(leftX, rightX), spawnY),
            Quaternion.identity);            
        }
        
    }
	
}
